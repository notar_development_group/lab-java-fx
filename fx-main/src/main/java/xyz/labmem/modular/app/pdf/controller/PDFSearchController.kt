package xyz.labmem.modular.app.pdf.controller
//
//import cn.hutool.core.io.FileUtil
//import cn.hutool.core.util.StrUtil
//import cn.hutool.db.nosql.mongo.MongoFactory
//import com.alibaba.fastjson.JSON
//import com.jfoenix.controls.JFXButton
//import com.jfoenix.controls.JFXListView
//import com.mongodb.BasicDBObject
//import com.mongodb.client.MongoDatabase
//import com.mongodb.client.model.Filters
//import javafx.application.Platform
//import javafx.collections.FXCollections
//import javafx.collections.ObservableList
//import javafx.concurrent.Task
//import javafx.event.ActionEvent
//import javafx.event.EventHandler
//import javafx.fxml.FXML
//import javafx.scene.control.Label
//import javafx.scene.control.TableColumn
//import javafx.scene.control.TableView
//import javafx.scene.control.TextField
//import javafx.scene.control.cell.PropertyValueFactory
//import javafx.scene.input.KeyCode
//import javafx.scene.input.MouseButton
//import javafx.scene.web.WebEvent
//import javafx.scene.web.WebView
//import javafx.stage.FileChooser
//import kotlinx.coroutines.DelicateCoroutinesApi
//import org.bson.Document
//import xyz.labmem.core.annotation.FXController
//import xyz.labmem.core.base.BaseController
//import xyz.labmem.core.exception.LabRunException
//import xyz.labmem.modular.app.pdf.entity.PdfViewEntity
//import xyz.labmem.tool.PdfConvertHtmlUtil
//import java.io.File
//import java.util.*
//import java.util.function.Consumer
//import java.util.regex.Pattern
//
///**
// * @Author lty
// * @Date 2021/9/22 17:49
// */
//@FXController(fxmlPath = "fxml/app/PDFSearch.fxml", title = "pdf搜索", icon = "icon.jpg", only = true)
//class PDFSearchController : BaseController() {
//
//    private val pdfListData = FXCollections.observableArrayList<String>()
//
//    private val selectData: ObservableList<PdfViewEntity> = FXCollections.observableArrayList()
//
//    private lateinit var db: MongoDatabase
//
//    private var imgView = false
//
//    private var nowEntity: PdfViewEntity? = null
//
//    @FXML
//    private var pdfList: JFXListView<String>? = null
//
//    @FXML
//    private var select: TableView<PdfViewEntity>? = null
//
//    @FXML
//    private var selectResult: TableColumn<PdfViewEntity, String>? = null
//
//    @FXML
//    private var search: TextField? = null
//
//    @FXML
//    private var page: Label? = null
//
//    @FXML
//    private var webView: WebView? = null
//
//    /**
//     * 配置
//     */
//    override fun InitStage() {
//        stage.isResizable = false
//    }
//
//    override fun onShow() {
//        //初始化MongoDB
//        loading.simpleTask(object : Task<Void?>() {
//            override fun call(): Void? {
//                val max = 1.0
//                try {
//                    updateMessage("正在初始化MongoDB..")
//                    updateProgress(0.4, max)
//                    val ds = MongoFactory.getDS("master")
//                    updateProgress(0.8, max)
//                    db = ds.getDb("pdf")
//                    refList()
//                } catch (e: Exception) {
//                    Platform.runLater {
//                        alertUtil.noticeError(e.message ?: "详情见日志！").text("MongoDB初始化失败").show()
//                    }
//                    throw LabRunException(e)
//                } finally {
//                    done()
//                }
//                return null
//            }
//        })
//    }
//
//    /**
//     * 初始化
//     */
//    override fun initialize() {
//        pdfList!!.items = pdfListData
//        select!!.items = selectData
//        selectResult!!.cellValueFactory = PropertyValueFactory("title")
//        //列表监听
//        pdfList!!.onMouseClicked = EventHandler {
//            if (it.button == MouseButton.PRIMARY) { //左键
//                loading.simpleTask(object : Task<Void?>() {
//                    override fun call(): Void? {
//                        try {
//                            val max = 1.0
//                            val item = pdfList!!.selectionModel.selectedItem
//                            if (StrUtil.isNotBlank(item)) {
//                                updateMessage("正在获取数据..")
//                                updateProgress(0.1, max)
//                                Thread.sleep(200)
//                                val ds = db.getCollection(item).find()
//                                updateMessage("打扫战场..")
//                                updateProgress(0.5, max)
//                                Thread.sleep(200)
//                                selectData.clear()
//                                updateMessage("数据载入..")
//                                val oneP = 0.5.div(ds.count())
//                                var progres = 0.5
//                                ds.forEach {
//                                    val entity = PdfViewEntity().apply {
//                                        content = it["content"].toString()
//                                        table = item
//                                        id = it["id"] as Int
//                                        base64 = it["base64"].toString()
//                                        title = if (content!!.trim().length > 30) content!!.trim().substring(0, 30)
//                                        else content!!.trim()
//                                    }
//                                    selectData.add(entity)
//                                    progres += oneP
//                                    updateProgress(progres, max)
//                                }
//                            }
//                        } catch (e: Exception) {
//                            Platform.runLater {
//                                alertUtil.noticeError(e.message ?: "详情见日志！").text("数据载入失败！").show()
//                            }
//                            throw LabRunException(e)
//                        } finally {
//                            Platform.runLater(select!!::refresh)
//                            done()
//                        }
//                        return null
//                    }
//                })
//            }
//        }
//
//        select!!.onMouseClicked = EventHandler {
//            val entity = select!!.selectionModel.selectedItem
//            entity?.let { showContent(it) }
//        }
//
//        //搜索框监听
//        search!!.onKeyPressed = EventHandler { event ->
//            if (event.code == KeyCode.ENTER && StrUtil.isNotBlank(search!!.text)) {
//                val value = search!!.text.trim()
//                selectData.clear()
//                loading.simpleTask(object : Task<Void?>() {
//                    override fun call(): Void? {
//                        try {
//                            updateMessage("正在搜索..")
//                            pdfListData.forEach(Consumer { d: String ->
//                                val pattern = Pattern.compile(value, Pattern.CASE_INSENSITIVE)
//                                val cond = BasicDBObject("content", pattern)
//                                val ds = db.getCollection(d).find(cond)
//                                ds.forEach(
//                                    Consumer { dt: Document ->
//                                        val entity = PdfViewEntity().apply {
//                                            content = dt["content"].toString()
//                                            table = d
//                                            id = dt["id"] as Int?
//                                            base64 = dt["base64"].toString()
//                                        }
//                                        var title = entity.content!!.trim()
//                                        var keyIndex = title.indexOf(value)
//                                        if (keyIndex == -1) {
//                                            //转小写查找
//                                            keyIndex = title.lowercase(Locale.getDefault()).indexOf(
//                                                value.lowercase(
//                                                    Locale.getDefault()
//                                                )
//                                            )
//                                            val source = entity.content!!.substring(keyIndex, value.length + keyIndex)
//                                            entity.content = entity.content!!
//                                                .replaceFirst(source, "<span id=\"point\" >$source</span>")
//                                                .replace(source, "<span  class=\"search\">$source</span>")
//
//                                        } else {
//                                            entity.content = entity.content!!
//                                                .replaceFirst(value, "<span id=\"point\" >$value</span>")
//                                                .replace(value, "<span  class=\"search\">$value</span>")
//
//                                        }
//                                        title = if (title.length - keyIndex > 30 && value.length < 30) title.substring(
//                                            keyIndex,
//                                            keyIndex + 30
//                                        ) else title.substring(keyIndex, keyIndex + value.length)
//                                        entity.title = title
//                                        selectData.add(entity)
//                                    } as Consumer<in Document>
//                                )
//                            })
//                        } catch (e: Exception) {
//                            Platform.runLater {
//                                alertUtil.noticeError(e.message ?: "详情见日志！").text("数据载入失败！").show()
//                            }
//                            throw LabRunException(e)
//                        } finally {
//                            Platform.runLater(select!!::refresh)
//                            done()
//                        }
//                        return null
//                    }
//                })
//            }
//        }
//    }
//
//    /**
//     * 导入pdf
//     *
//     * @param event
//     */
//    @FXML
//    fun importPDF(event: ActionEvent) {
//        val fileChooser = FileChooser()
//        fileChooser.extensionFilters
//            .add(FileChooser.ExtensionFilter("PDF", "*.pdf"))
//        val file = fileChooser.showOpenDialog(getWin())
//        if (file != null) {
//            val tableName = FileUtil.getName(file)
//            if (pdfListData.contains(tableName)) {
//                alert.apply {
//                    setCancelBtn("取消")
//                    setSureBtn("确定", { importUpdateAction(file, tableName) }, true)
//                    title = "警告"
//                    message = "当前PDF已导入，是否覆盖？"
//                    create()
//                }
//            } else importAction(file, tableName)
//        }
//    }
//
//    @DelicateCoroutinesApi
//    private fun importAction(file: File, TableName: String) {
//        loading.simpleTask(object : Task<Void?>() {
//            override fun call(): Void? {
//                try {
//                    val max = 1.0
//                    updateMessage("正在解析pdf..")
//                    updateProgress(0.1, max)
//                    //解析PDF
//                    val entities = PdfConvertHtmlUtil.pdfStreamToEntity(file)
//                    if (entities != null) {
//                        updateMessage("正在导入数据..")
//                        updateProgress(0.5, max)
//                        val onep = 0.5.div(entities.size)
//                        var progres = 0.5
//                        //创建数据表
//                        db.createCollection(TableName)
//                        entities.forEach {
//                            db.getCollection(TableName).insertOne(Document.parse(JSON.toJSONString(it)))
//                            progres += onep
//                            updateProgress(progres, max)
//                        }
//                    }
//                } catch (e: Exception) {
//                    Platform.runLater {
//                        alertUtil.noticeError(e.message ?: "详情见日志！").text("PDF导入失败！").show()
//                    }
//                    throw LabRunException(e)
//                } finally {
//                    refList()
//                    done()
//                }
//                return null
//            }
//        })
//    }
//
//    @DelicateCoroutinesApi
//    private fun importUpdateAction(file: File, TableName: String) {
//        loading.simpleTask(object : Task<Void?>() {
//            override fun call(): Void? {
//                try {
//                    val max = 1.0
//                    updateMessage("正在解析pdf..")
//                    updateProgress(0.1, max)
//                    //解析PDF
//                    val entities = PdfConvertHtmlUtil.pdfStreamToEntity(file)
//                    if (entities != null) {
//                        db.getCollection(TableName)
//                        updateMessage("正在覆盖数据..")
//                        updateProgress(0.5, max)
//                        val onep = 0.5.div(entities.size)
//                        var progres = 0.5
//                        entities.forEach { e ->
//                            db.getCollection(TableName).updateOne(
//                                Filters.eq("id", e.id),
//                                Document("\$set", Document("content", e.content).append("base64", e.base64))
//                            )
//                            progres += onep
//                            updateProgress(progres, max)
//                        }
//                    }
//                } catch (e: Exception) {
//                    Platform.runLater {
//                        alertUtil.noticeError(e.message ?: "详情见日志！").text("PDF覆盖失败！").show()
//                    }
//                    throw LabRunException(e)
//                } finally {
//                    refList()
//                    done()
//                }
//                return null
//            }
//        })
//    }
//
////    @FXML
////    fun recording(event: ActionEvent) {
////        val that = event.source as JFXButton
////        when (that.text) {
////            "录音" -> {
////                that.text = "停止"
////                that.style = "-fx-background-color:rgb(247,137,137)"
////            }
////            "停止" -> {
////                that.text = "录音"
////                that.style = "-fx-background-color:rgb(115,180,115)"
////            }
////        }
////        successAlert(that.text)
////    }
//
//    /**
//     * 删除PDF
//     */
//    @DelicateCoroutinesApi
//    fun delPDF(even: ActionEvent) {
//        val table = pdfList!!.selectionModel.selectedItem
//        if (StrUtil.isNotBlank(table)) {
//            alert.apply {
//                setCancelBtn("取消")
//                setSureBtn("确定", {
//                    loading.simpleTask(object : Task<Void?>() {
//                        override fun call(): Void? {
//                            try {
//                                db.getCollection(table).drop()
//                            } catch (e: Exception) {
//                                Platform.runLater {
//                                    alertUtil.noticeError(e.message ?: "详情见日志！").text("删除PDF失败！").show()
//                                }
//                                throw LabRunException(e)
//                            } finally {
//                                refList()
//                                done()
//                            }
//                            return null
//                        }
//                    })
//                }, true)
//                title = "警告"
//                message = "是否删除《$table》?"
//                create()
//            }
//        }
//    }
//
//    @FXML
//    fun up(even: ActionEvent?) {
//        if (nowEntity != null) {
//            val cond = BasicDBObject("id", nowEntity!!.id!! - 1)
//            val ds = db.getCollection(nowEntity!!.table!!).find(cond)
//            ds.forEach(Consumer<Document> { dt: Document ->
//                val entity = PdfViewEntity().apply {
//                    content = dt["content"].toString()
//                    table = nowEntity!!.table
//                    id = dt["id"] as Int
//                    base64 = dt["base64"].toString()
//                }
//                showContent(entity)
//            } as Consumer<in Document>)
//        }
//    }
//
//    @FXML
//    fun down(even: ActionEvent?) {
//        if (nowEntity != null) {
//            val cond = BasicDBObject("id", nowEntity!!.id!! + 1)
//            val ds = db.getCollection(nowEntity!!.table!!).find(cond)
//            ds.forEach(Consumer<Document> { dt: Document ->
//                val entity = PdfViewEntity().apply {
//                    content = dt["content"].toString()
//                    table = nowEntity!!.table
//                    id = dt["id"] as Int
//                    base64 = dt["base64"].toString()
//                }
//                showContent(entity)
//            } as Consumer<in Document>)
//        }
//    }
//
//    @FXML
//    fun switchView(event: ActionEvent) {
//        val that = event.source as JFXButton
//        when (that.text) {
//            "原图" -> {
//                that.text = "原文"
//                that.style = "-fx-background-color:rgb(115,180,115)"
//                imgView = true
//            }
//            "原文" -> {
//                that.text = "原图"
//                that.style = "-fx-background-color:rgb(247,137,137)"
//                imgView = false
//            }
//        }
//        showContent(nowEntity)
//    }
//
//    private fun refList() {
//        Platform.runLater {
//            pdfListData.clear()
//            try {
//                db.listCollectionNames().forEach(Consumer { e: String ->
//                    pdfListData.add(e)
//                } as Consumer<in String>)
//            }catch (e:Exception){
//                e.printStackTrace()
//            }
//            pdfList!!.refresh()
//        }
//    }
//
//    private fun showContent(entity: PdfViewEntity?) {
//        entity?.let {
//            nowEntity = it
//            page!!.text = "第" + (it.id!! + 1).toString() + " 页"
//            pdfList!!.selectionModel.select(it.table)
//            pdfList!!.scrollTo(it.table)
//            val webEngine = webView!!.engine
//            webEngine.isJavaScriptEnabled = true
//            val html = StringBuilder()
//            html.append(
//                "<html>" +
//                        "<head>" +
//                        "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">" +
//                        "<style>" +
//                        "        .search{" +
//                        "            color:red" +
//                        "        }" +
//                        "        .content{" +
//                        "            font-size: 14px;word-break:break-all;word-wrap:break-word;width:100%;" +
//                        "         }" +
//                        "    </style>" +
//                        "</head>" +
//                        "<body>"
//            )
//            if (imgView) html.append("<img width=1010 src=\"data:image/png;base64,").append(it.base64)
//                .append("\" />") else html.append("\"<div class=\"content\"><pre>").append(it.content)
//                .append("</pre></div>\"")
//            html.append(
//                ("</body>" +
//                        "<script>" +
//                        "(function () {" +
//                        "        setTimeout(() => {" +
//                        "            var hTopDom = document.getElementById('point');" +
//                        "            var hTop = hTopDom.offsetTop;" +
//                        "            document.body.scrollTop = hTop;" +
//                        "        }, 200);" +
//                        "    })()" +
//                        "</script>" +
//                        "</html>")
//            )
//            webEngine.loadContent(html.toString())
//            webEngine.onAlert = EventHandler { event: WebEvent<String> ->
//                alertUtil.noticeSuccess(
//                    event.data
//                ).show()
//            }
//        }
//    }
//
//}