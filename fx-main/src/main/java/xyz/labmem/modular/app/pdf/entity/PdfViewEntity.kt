package xyz.labmem.modular.app.pdf.entity

/**
 * @Author lty
 * @Date 2021/9/22 18:00
 */
class PdfViewEntity {

    var id: Int? = null

    var table: String? = null

    var content: String? = null

    var title: String? = null

    var base64: String? = null

}