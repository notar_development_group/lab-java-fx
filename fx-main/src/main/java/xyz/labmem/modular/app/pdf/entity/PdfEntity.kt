package xyz.labmem.modular.app.pdf.entity

/**
 * @Author lty
 * @Date 2021/9/22 18:01
 */
class PdfEntity {

    var id: Int? = null

    var content: String? = null

    var base64: String? = null
}