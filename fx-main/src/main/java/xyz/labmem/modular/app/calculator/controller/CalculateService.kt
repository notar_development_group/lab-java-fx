package xyz.labmem.modular.app.calculator.controller

import javafx.application.Platform
//import xyz.labmem.core.util.gui.alert.AlertUtil
import java.math.BigDecimal
import java.math.MathContext

/**
 * @Author lty
 * @Date 2021/9/24 10:11
 */
class CalculateService {

    var num = BigDecimal(0)

    var ope: String? = null

    fun import(num: BigDecimal, ope: String) {
        this.num = num
        this.ope = ope
    }

    fun calculation(num2: BigDecimal): BigDecimal {
        var count = BigDecimal(0)
        if (ope != null) {
            when (ope) {
                "%" -> {
                }
                "+" -> {
                    count = num.add(num2)
                }
                "-" -> {
                    count = if (num == BigDecimal(0))
                        num2
                    else
                        num.subtract(num2)
                }
                "x" -> {
                    count = if (num == BigDecimal(0))
                        num2
                    else
                        num.multiply(num2)
                }
                "÷" -> {
                    if (num2 == BigDecimal(0))
//                        Platform.runLater { AlertUtil().noticeError("被除数不能为0").show() }
                    else
                        count = num.divide(num2)
                }
            }
        }
        this.num = count
        return count
    }

}