package xyz.labmem.modular.app.calculator.controller
//
//import com.jfoenix.controls.JFXButton
//import com.jfoenix.controls.JFXTextField
//import javafx.animation.FadeTransition
//import javafx.beans.value.ChangeListener
//import javafx.event.ActionEvent
//import javafx.event.EventHandler
//import javafx.fxml.FXML
//import javafx.scene.control.Label
//import javafx.scene.image.Image
//import javafx.scene.image.ImageView
//import javafx.scene.input.KeyCode
//import javafx.scene.layout.Background
//import javafx.scene.layout.HBox
//import javafx.scene.layout.Pane
//import javafx.scene.layout.VBox
//import javafx.scene.paint.Paint
//import javafx.stage.Modality
//import javafx.stage.StageStyle
//import javafx.util.Duration
//import xyz.labmem.core.annotation.FXController
//import xyz.labmem.core.base.BaseController
//import xyz.labmem.core.util.getResourceFile
//import xyz.labmem.core.util.gui.dragWin
//import java.math.BigDecimal
//
//
///**
// * 计算器
// * @Author lty
// * @Date 2021/9/23 11:45
// */
//@FXController(title = "计算器", fxmlPath = "fxml/app/calculator.fxml")
//class CalculatorController : BaseController() {
//
//    private val calculate = CalculateService()
//
//    @FXML
//    var topDrag: HBox? = null   //状态栏
//
//    @FXML
//    var winHide: JFXButton? = null  //最小化
//
//    @FXML
//    var winClose: JFXButton? = null     //关闭
//
//    @FXML
//    var moreBtn: JFXButton? = null     //更多按钮
//
//    @FXML
//    var historyBtn: JFXButton? = null      //历史按钮
//
//    @FXML
//    var backspace: JFXButton? = null      //删除按钮
//
//    @FXML
//    var equalBtn: JFXButton? = null      //等于按钮
//
//    @FXML
//    var contentV: VBox? = null     //内容盒子
//
//    @FXML
//    var depositText: JFXTextField? = null     //寄存
//
//    @FXML
//    var calculateText: Label? = null   //  计算区
//
//    override fun InitStage() {
//        stage.dragWin(topDrag!!)
//        stage.scene.fill = null
//        root.apply {
//            children.forEach {
//                if (it is Pane) {
//                    it.background = Background.EMPTY
//                }
//            }
//            background = Background.EMPTY
//        }
//        /**
//         * 监听焦点改变背景颜色
//         */
//        stage.focusedProperty().addListener(ChangeListener { observableValue, _, _ ->
//            if (observableValue.value) {
//                root.style = "-fx-background-radius:10px;-fx-opacity: 0.9;-fx-background-color: rgb(195,193,193);"
//            } else
//                root.style = "-fx-background-radius:10px;-fx-opacity: 0.9;-fx-background-color: rgb(230,230,230);"
//        })
//        //按键绑定
//        stage.scene.setOnKeyPressed {
//            when (it.code) {
//                KeyCode.NUMPAD1 -> input(it.text)
//                KeyCode.NUMPAD2 -> input(it.text)
//                KeyCode.NUMPAD3 -> input(it.text)
//                KeyCode.NUMPAD4 -> input(it.text)
//                KeyCode.NUMPAD5 -> input(it.text)
//                KeyCode.NUMPAD6 -> input(it.text)
//                KeyCode.NUMPAD7 -> input(it.text)
//                KeyCode.NUMPAD8 -> input(it.text)
//                KeyCode.NUMPAD9 -> input(it.text)
//                KeyCode.NUMPAD0 -> input(it.text)
//                KeyCode.DECIMAL -> input(it.text)
//                KeyCode.ADD -> calculation(it.text)
//                KeyCode.SUBTRACT -> calculation(it.text)
//                KeyCode.MULTIPLY -> calculation("x")
//                KeyCode.DIVIDE -> calculation("÷")
//                KeyCode.BACK_SPACE -> {
//                    val dt = depositText!!.text
//                    if (dt.length > 1)
//                        depositText!!.text = dt.substring(0, dt.length - 1)
//                    else
//                        depositText!!.text = "0"
//                    dep = depositText!!.text
//                }
//                KeyCode.ENTER -> calculate()
//                else -> {
//                }
//            }
//        }
//        depositText!!.selectHome()
//        stage.apply {
//            initStyle(StageStyle.TRANSPARENT)
//            initModality(Modality.WINDOW_MODAL)
//        }
//    }
//
//    override fun initialize() {
//
//        //状态栏按钮绑定
//        winHide!!.apply {
//            onAction = EventHandler {
//                FadeTransition(Duration.millis(400.0)).apply {
//                    fromValue = 1.0 // 设置起始透明度为1.0，表示不透明
//                    toValue = 0.0 // 设置结束透明度为0.0，表示透明
//                    node = root // 设置动画应用的节点
//                    onFinished = EventHandler {
//                        stage.isIconified = true
//                    }
//                    play() // 播放动画
//                }
//            }
//            var btnStyle: String? = null
//            onMouseEntered = EventHandler {
//                btnStyle = this.style
//                this.style = "-fx-background-color: rgb(179,180,180)"
//            }
//            onMouseExited = EventHandler {
//                this.style = btnStyle
//            }
//        }
//        winClose!!.apply {
//            onAction = EventHandler {
//                FadeTransition(Duration.millis(400.0)).apply {
//                    fromValue = 1.0 // 设置起始透明度为1.0，表示不透明
//                    toValue = 0.0 // 设置结束透明度为0.0，表示透明
//                    node = root // 设置动画应用的节点
//                    onFinished = EventHandler {
//                        stage.close()
//                    }
//                    play() // 播放动画
//                }
//            }
//            var btnFill: Paint? = null
//            var btnStyle: String? = null
//            onMouseEntered = EventHandler {
//                btnFill = this.textFill
//                btnStyle = this.style
//                this.textFill = Paint.valueOf("WHITE")
//                this.style = "-fx-background-color: rgb(232,17,35)"
//            }
//            onMouseExited = EventHandler {
//                this.textFill = btnFill
//                this.style = btnStyle
//            }
//        }
//        contentV!!.children.forEach { n ->
//            if (n is HBox) {
//                n.children.forEach {
//                    if (it is JFXButton) {
//                        it.apply {
//                            var btnStyle: String? = null
//                            onMouseEntered = EventHandler {
//                                btnStyle = this.style
//                                this.style =
//                                    "-fx-background-color: rgb(187,185,181);-fx-border-width: 1 1 1 1;-fx-border-style: solid;-fx-border-color: BLACK"
//                            }
//                            onMouseExited = EventHandler {
//                                this.style = btnStyle
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        equalBtn!!.apply {
//            var btnStyle: String? = null
//            onMouseEntered = EventHandler {
//                btnStyle = this.style
//                this.style = "-fx-background-color: rgb(59,142,207)"
//            }
//            onMouseExited = EventHandler {
//                this.style = btnStyle
//            }
//        }
//        //更多按钮绑定
//        moreBtn!!.apply {
//            text = ""
//            val morePng = ImageView(Image(getResourceFile("img/app/calculator/more.png").stream))
//            morePng.fitWidth = 15.0
//            morePng.fitHeight = 15.0
//            graphic = morePng
//        }
//        historyBtn!!.apply {
//            text = ""
//            val morePng = ImageView(Image(getResourceFile("img/app/calculator/history.png").stream))
//            morePng.fitWidth = 15.0
//            morePng.fitHeight = 15.0
//            graphic = morePng
//        }
//        backspace!!.apply {
//            text = ""
//            val morePng = ImageView(Image(getResourceFile("img/app/calculator/backspace.png").stream))
//            morePng.fitWidth = 22.0
//            morePng.fitHeight = 22.0
//            graphic = morePng
//            onAction = EventHandler {
//                val dt = depositText!!.text
//                if (dt.length > 1)
//                    depositText!!.text = dt.substring(0, dt.length - 1)
//                else
//                    depositText!!.text = "0"
//                dep = depositText!!.text
//            }
//        }
//        //等于号绑定
//        equalBtn?.let { i ->
//            i.onAction = EventHandler {
//                calculate()
//            }
//        }
//    }
//
//    private fun calculation(ope: String) {
//        depositText?.let {
//            if (calculate.ope == null) {
//                calculate.import(BigDecimal(dep), ope)
//            } else {
//                if (calculate.ope == ope && change) {
//                    calculate.calculation(BigDecimal(dep)).toString()
//                } else {
//                    calculate.ope = ope
//                }
//            }
//            calculateText?.let { c ->
//                c.text = "${calculate.num}  $ope "
//            }
//            it.text = calculate.num.toString()
//            dep = "0"
//        }
//        change = false
//    }
//
//    private fun input(num: String) {
//        depositText?.let {
//            var deposit = dep
//            if (num == ".") {
//                if (deposit.contains("."))
//                    return
//            }
//            if (deposit != "0" || num == ".") {
//                deposit += num
//            } else
//                deposit = num
//            dep = if (deposit.length > 14) BigDecimal(deposit).toString() else deposit
//            it.text = dep
//        }
//        change = true
//    }
//
//    ////////////////////////////////////////////////////////////
//
//    private var dep = "0"
//
//    private var change = false
//
//    fun calculate() {
//        calculateText?.let {
//            depositText?.let { d ->
//                if (calculate.ope == null)
//                    it.text = dep
//                else
//                    it.text = "${calculate.num} ${calculate.ope} $dep = "
//                if (dep == "0" && d.text != "0")
//                    d.text = calculate.calculation(BigDecimal(d.text)).toString()
//                else
//                    d.text = calculate.calculation(BigDecimal(dep)).toString()
//            }
//        }
//    }
//
//    @FXML//清楚所有记忆
//    fun mc_b(event: ActionEvent) {
//
//    }
//
//    @FXML//记忆读出
//    fun mr_b(event: ActionEvent) {
//
//    }
//
//    @FXML//记忆加法
//    fun mA_b(event: ActionEvent) {
//
//    }
//
//    @FXML//记忆减法
//    fun mB_b(event: ActionEvent) {
//
//    }
//
//    @FXML//记忆储存
//    fun ms_b(event: ActionEvent) {
//
//    }
//
//    @FXML//记忆
//    fun mm_b(event: ActionEvent) {
//
//    }
//
//    @FXML//百分号
//    fun percent(event: ActionEvent) {
//
//    }
//
//    @FXML//清除寄存器
//    fun clearE(event: ActionEvent) {
//        dep = "0"
//        depositText?.let { it.text = "0" }
//    }
//
//    @FXML//清空
//    fun clear(event: ActionEvent) {
//        dep = "0"
//        change = false
//        calculate.num = BigDecimal(0)
//        calculateText?.let { it.text = "" }
//        depositText?.let { it.text = "0" }
//    }
//
//    @FXML//加
//    fun add(event: ActionEvent) {
//        calculation("+")
//    }
//
//    @FXML//减
//    fun sub(event: ActionEvent) {
//        calculation("-")
//    }
//
//    @FXML//乘
//    fun multi(event: ActionEvent) {
//        calculation("x")
//    }
//
//    @FXML//除
//    fun divide(event: ActionEvent) {
//        calculation("÷")
//    }
//
//    @FXML//数字1
//    fun n1(event: ActionEvent) {
//        input("1")
//    }
//
//    @FXML//数字2
//    fun n2(event: ActionEvent) {
//        input("2")
//    }
//
//    @FXML//数字3
//    fun n3(event: ActionEvent) {
//        input("3")
//    }
//
//    @FXML//数字4
//    fun n4(event: ActionEvent) {
//        input("4")
//    }
//
//    @FXML//数字5
//    fun n5(event: ActionEvent) {
//        input("5")
//    }
//
//    @FXML//数字6
//    fun n6(event: ActionEvent) {
//        input("6")
//    }
//
//    @FXML//数字7
//    fun n7(event: ActionEvent) {
//        input("7")
//    }
//
//    @FXML//数字8
//    fun n8(event: ActionEvent) {
//        input("8")
//    }
//
//    @FXML//数字9
//    fun n9(event: ActionEvent) {
//        input("9")
//    }
//
//    @FXML//数字0
//    fun n0(event: ActionEvent) {
//        input("0")
//    }
//
//    @FXML//·
//    fun nd(event: ActionEvent) {
//        input(".")
//    }
//
//
//}