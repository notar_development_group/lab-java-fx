package xyz.labmem.modular.main.controller

import io.github.palexdev.materialfx.controls.MFXButton
import io.github.palexdev.materialfx.controls.MFXTextField
import javafx.event.EventHandler
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import javafx.scene.web.WebView
import xyz.labmem.core.annotation.FXController
import xyz.labmem.core.base.BaseController

/**
 * @description: do something
 * @author: liutianyu
 * @date: 2022/11/17 17:50
 */
@FXController(title = "webkit 测试", only = true)
class NetBrowserStage : BaseController() {

    private val webView = WebView()

    private val engine = webView.engine

    private val url = MFXTextField()

    private val en = MFXButton("转到").apply {

        onAction = EventHandler {
            engine.load(url.text)
        }
    }

    override fun InitStage() {
//        url.prefHeight = 50.0
        url.prefWidth = 500.0
        stage.isResizable = true
        root.prefHeightProperty().bind(stage.heightProperty())
        root.prefWidthProperty().bind(stage.widthProperty())
        root.children.apply {
            add(VBox().apply {
                children.add(HBox().apply {
                    children.add(url)
                    children.add(en)
                })
                children.add(webView.apply {
                    prefWidthProperty().bind(root.widthProperty())
                    prefHeightProperty().bind(root.heightProperty().subtract(url.prefHeight))
                })
            })
        }

    }

    override fun onShow() {
        val redirectController = (request.redirectController as MainController)
        redirectController.stage.hide()
        stage.setOnCloseRequest {
            redirectController.stage.show()
        }
    }
}