package xyz.labmem.modular.main.aspect

import cn.hutool.core.lang.Console
import xyz.labmem.core.aspect.BaseFXAspect
import java.lang.reflect.Method

/**
 * @Author lty
 * @Date 2021/9/16 10:51
 */
class MainAspect : BaseFXAspect {

    override fun before(target: Any, method: Method, args: Array<out Any>?): Boolean {
        Console.log("切切切面 --- before Method [${target.javaClass.name}.${method.name}] execute args: $args")
        return true
    }

    override fun after(target: Any, method: Method, args: Array<out Any>?, returnVal: Any?): Boolean {
        Console.log("切切切面 --- after Method [${target.javaClass.name}.${method.name}] execute returnVal: $returnVal")
        return true
    }
}