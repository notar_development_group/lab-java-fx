package xyz.labmem.modular.main.controller

import javafx.stage.StageStyle
import xyz.labmem.core.annotation.FXController
import xyz.labmem.core.annotation.FXLoadController
import xyz.labmem.core.base.BaseController
import xyz.labmem.core.util.getResourceFile


/**
 * @Author lty
 * @Date 2021/9/13 10:39
 */
@FXController(title = "App Loading..", icon = "icon.jpg")
@FXLoadController
class AppLoadingController : BaseController() {

    override fun InitStage() {
        root.scene.apply {
            fill = null
            stylesheets.add(getResourceFile("css/appLoading.css").url.toExternalForm());
        }
        stage.apply {
            isAlwaysOnTop = true
            width = 366.0
            height = 180.0
            initStyle(StageStyle.TRANSPARENT)
        }
    }

    /**
     * 加载初始化操作
     */
    override fun loadingInit() {
        Thread.sleep(1000)
    }

    override fun initialize() {
        super.initialize()
    }
}