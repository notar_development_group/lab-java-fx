package xyz.labmem.modular.main.controller

import io.github.palexdev.materialfx.controls.MFXButton
import io.github.palexdev.materialfx.enums.ButtonType
import javafx.concurrent.Task
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.geometry.Insets
import javafx.scene.control.Label
import javafx.scene.control.Menu
import javafx.scene.control.MenuBar
import javafx.scene.control.MenuItem
import javafx.scene.layout.HBox
import javafx.scene.layout.Pane
import javafx.scene.layout.VBox
import javafx.scene.text.Font
import javafx.scene.text.FontWeight
import javafx.scene.text.TextAlignment
import javafx.stage.FileChooser
import kotlinx.coroutines.DelicateCoroutinesApi
import org.controlsfx.control.GridView
import xyz.labmem.core.annotation.FXController
import xyz.labmem.core.base.BaseController
import xyz.labmem.core.base.FXRedirectParam
import xyz.labmem.core.reflex.FXApplication
import xyz.labmem.core.util.gui.alert.Alert
import xyz.labmem.core.util.gui.alert.AlertType
import xyz.labmem.core.util.gui.loading.LoadingBuild
import xyz.labmem.core.util.gui.loading.LoadingType
import xyz.labmem.core.util.gui.notice.Notice
import xyz.labmem.core.util.gui.notice.SimpleNotification
import xyz.labmem.core.util.gui.tray.DefaultSystemTray
import java.awt.Desktop
import java.awt.EventQueue
import java.io.IOException


/**
 * @Author lty
 * @Date 2021/9/14 8:55
 */
@FXController(
//    fxmlPath = "fxml/main.fxml",
    title = "LabApp",
    icon = "icon.jpg",
    main = true,
    tray = true,
    loadContrName = "AppLoadingController",
)
//@FXAspect(MainAspect::class)
class MainController : BaseController() {

    private val desktop = Desktop.getDesktop()

    @FXML
    var menu: MenuBar? = null

    @OptIn(DelicateCoroutinesApi::class)
    override fun InitStage() {
        stage.isResizable = false
        root.prefHeight = 800.0
        root.prefWidth = 1400.0
        root.children.add(VBox().apply {
            prefHeight = 786.0
            prefWidth = 1400.0
            children.add(MenuBar().apply {
                prefHeight = 25.0
                menus.add(Menu().apply {
                    text = "操作"
                    items.add(MenuItem().apply {
                        text = "打开"
                        onAction = EventHandler { openFile(it) }
                    })
                })
                menus.add(Menu().apply {
                    text = "帮助"
                    items.add(MenuItem().apply {
                        text = "退出"
                        onAction = EventHandler { FXApplication.close() }
                    })
                })
            })
            children.add(Pane().apply {
                children.add(VBox().apply {
                    children.add(HBox().apply {
                        prefHeight = 192.0
                        prefWidth = 673.0
                        children.add(Label("组件展示:").apply {
                            HBox.setMargin(this, Insets(30.0, 10.0, 0.0, 10.0))
                            font = Font.font(18.0)
                        })
                        children.add(MFXButton().apply {
                            setPrefSize(70.0, 40.0)
                            buttonType = ButtonType.RAISED
                            style = "-fx-background-color: -mfx-green;-fx-text-fill: white"
                            text = "成功"
                            font = Font.font("成功", FontWeight.BOLD, 17.0)
                            onAction = EventHandler {
                                notice.msg("111111111111111111111111111111",AlertType.SUCCESS)
                            }
                            HBox.setMargin(this, Insets(20.0, 0.0, 0.0, 10.0))
                        })
                        children.add(MFXButton().apply {
                            setPrefSize(70.0, 40.0)
                            buttonType = ButtonType.RAISED
                            style = "-fx-background-color: -mfx-orange;-fx-text-fill: white"
                            text = "失败"
                            font = Font.font("失败", FontWeight.BOLD, 17.0)
                            onAction = EventHandler {
                                Alert(getWin()).toFailed("操作失败！").show()
                            }
                            HBox.setMargin(this, Insets(20.0, 0.0, 0.0, 10.0))
                        })
                        children.add(MFXButton().apply {
                            setPrefSize(70.0, 40.0)
                            buttonType = ButtonType.RAISED
                            style = "-fx-background-color: -mfx-red;-fx-text-fill: white"
                            text = "错误"
                            font = Font.font("错误", FontWeight.BOLD, 17.0)
                            onAction = EventHandler {
                                Alert(getWin()).toError("系统错误！").show()
                            }
                            HBox.setMargin(this, Insets(20.0, 0.0, 0.0, 10.0))
                        })
                        children.add(MFXButton().apply {
                            setPrefSize(70.0, 40.0)
                            buttonType = ButtonType.RAISED
                            style = "-fx-background-color: -mfx-blue;-fx-text-fill: white"
                            text = "消息"
                            font = Font.font("消息", FontWeight.BOLD, 17.0)
                            onAction = EventHandler {
                                Alert(getWin()).apply {
                                    open(
                                        "提示",
                                        Pane().apply pane@{
                                            setPrefSize(300.0, 100.0)
                                            children.add(Label("这是一个弹窗").apply {
                                                prefWidthProperty().bind(this@pane.widthProperty())
                                                prefHeightProperty().bind(this@pane.heightProperty())
                                                textAlignment = TextAlignment.CENTER
                                                font = Font.font(18.0)
                                            })
                                        },
                                        arrayOf(
                                            MFXButton("确定").apply {
                                                buttonType = ButtonType.RAISED
                                                style = "-fx-background-color: -mfx-green;-fx-text-fill: white"
                                                onAction = EventHandler {
//                                                    close()
                                                    LoadingBuild.buildTypeTwo(getWin())
                                                        .loadingType(LoadingType.BARS)
                                                        .make(object : Task<Void>() {
                                                            override fun call(): Void? {
                                                                val max = 1.0
                                                                updateMessage("别来无恙啊.")
                                                                Thread.sleep(3000)
                                                                updateProgress(0.05, max)
                                                                Thread.sleep(2000)
                                                                updateProgress(0.3, max)
                                                                updateMessage("马上就好了.")
                                                                Thread.sleep(1000)
                                                                updateProgress(0.5, max)
                                                                Thread.sleep(1000)
                                                                updateProgress(0.8, max)
                                                                Thread.sleep(1000)
                                                                updateProgress(0.99, max)
                                                                updateMessage("尽情享用吧.")
                                                                Thread.sleep(800)
                                                                updateProgress(1.0, max)
                                                                done()
                                                                return null
                                                            }
                                                        }) {
                                                            close()
                                                        }
                                                }
                                            },
                                            MFXButton("取消").apply {
                                                buttonType = ButtonType.RAISED
                                                style = "-fx-background-color: -mfx-red;-fx-text-fill: white"
                                                onAction = EventHandler { close() }
                                            }
                                        )
                                    )
                                }
                            }
                            HBox.setMargin(this, Insets(20.0, 0.0, 0.0, 10.0))
                        })
                        children.add(MFXButton().apply {
                            setPrefSize(120.0, 40.0)
                            buttonType = ButtonType.RAISED
                            style = "-fx-background-color: -mfx-orange;-fx-text-fill: white"
                            text = "模态加载"
                            font = Font.font("模态加载", FontWeight.BOLD, 17.0)
                            onAction = EventHandler {
                                LoadingBuild.buildTypeTwo(getWin())
                                    .setMsg("这是一个模态加载，3秒")
                                    .make {
                                        Thread.sleep(3000)
                                    }
                            }
                            HBox.setMargin(this, Insets(20.0, 0.0, 0.0, 10.0))
                        })
                        children.add(MFXButton().apply {
                            setPrefSize(90.0, 40.0)
                            buttonType = ButtonType.RAISED
                            style = "-fx-background-color: -mfx-yellow;-fx-text-fill: white"
                            text = "通知"
                            font = Font.font("通知", FontWeight.BOLD, 17.0)
                            onAction = EventHandler {
                                notice.create(
                                    content = HBox(
                                        Label(
                                            """
                                    hello world
                                    hello world
                                    hello world
                                    hello world
                                """.trimIndent()
                                        )
                                    ).apply {
                                        prefHeight = 100.0
                                        prefWidth = 300.0
                                    }, stage
                                )
                            }
                            HBox.setMargin(this, Insets(20.0, 0.0, 0.0, 10.0))
                        })

                        children.add(MFXButton().apply {
                            setPrefSize(90.0, 40.0)
                            buttonType = ButtonType.RAISED
                            style = "-fx-background-color: -mfx-yellow;-fx-text-fill: white"
                            text = "消息"
                            font = Font.font("消息", FontWeight.BOLD, 17.0)
                            onAction = EventHandler {
                                (tray as DefaultSystemTray).winMsg("123","321")
                            }
                            HBox.setMargin(this, Insets(20.0, 0.0, 0.0, 10.0))
                        })
                    })
                    children.add(GridView<MFXButton>().apply {
                        prefHeight = 192.0
                        prefWidth = 673.0
                        children.add(MFXButton().apply {
                            setPrefSize(90.0, 40.0)
                            buttonType = ButtonType.RAISED
                            style = "-fx-background-color: -mfx-red;-fx-text-fill: white"
                            text = "WEBKIT"
                            font = Font.font("WEBKIT", FontWeight.BOLD, 17.0)
                            onAction = EventHandler {
                                redirect(FXRedirectParam(NetBrowserStage::class))
                            }
                            HBox.setMargin(this, Insets(20.0, 0.0, 0.0, 10.0))
                        })
                    })

                })
            })
        })
    }

//    fun convertDialogTo(styleClass: String?) {
//        dialogContent!!.styleClass.removeIf { s: String -> s == "mfx-info-dialog" || s == "mfx-warn-dialog" || s == "mfx-error-dialog" }
//        if (styleClass != null) dialogContent!!.styleClass.add(styleClass)
//    }

    //    @FXML
//    fun error(event: ActionEvent) {
//        alertUtil.errorAlert().apply {
//            CancelBtnBackFun = {
//                alertUtil.noticeError("确认错误！")
//                    .owner(stage)
//                    .show()
//            }
//            create()
//        }
//    }
//
//    @FXML
//    fun failed(event: ActionEvent) {
//        alertUtil.failedAlert().apply {
//            CancelBtnBackFun = {
//                alertUtil.noticeFailed("确认失败！")
//                    .owner(stage)
//                    .show()
//            }
//            create()
//        }
//    }
//
//    @FXML
//    fun success(event: ActionEvent) {
//        alertUtil.successAlert().apply {
//            CancelBtnBackFun = {
//                alertUtil.noticeSuccess("确认成功！")
//                    .owner(stage)
//                    .show()
//            }
//            create()
//        }
//    }
//
//    @FXML
//    @DelicateCoroutinesApi
//    fun loading(event: ActionEvent) {
////        val task = object : Task<Void>() {
////            override fun call(): Void? {
////                val max = 1.0
////                updateMessage("别来无恙啊.")
////                Thread.sleep(1000)
////                updateProgress(0.05, max)
////                Thread.sleep(2000)
////                updateProgress(0.3, max)
////                updateMessage("马上就好了.")
////                Thread.sleep(1000)
////                updateProgress(0.5, max)
////                Thread.sleep(1000)
////                updateProgress(0.8, max)
////                Thread.sleep(1000)
////                updateProgress(0.9, max)
////                updateMessage("尽情享用吧.")
////                Thread.sleep(800)
////                updateProgress(1.0, max)
////                done()
////                return null
////            }
////        }
////        loading.simpleTask(task)
//        loading.apply {
//            getMasker().text = "处理协程中请稍等.."
//            showProgressBar = false
//            show {
//                Thread.sleep(5000)
//            }
//        }
//        Console.log("执行协程")
//    }
//
//    @FXML
//    fun about(event: ActionEvent) {
//        alert.apply {
//            message = "JAVAFX 小程序 \n version 0.1"
//            title = "关于"
//            create()
//        }
//    }
//
//    @FXML
//    fun exit(event: ActionEvent) {
//        alert.apply {
//            setCancelBtn("取消")
//            setSureBtn("确定", FXApplication::close)
//            title = "警告"
//            message = "是否要退出？"
//            create()
//        }
//    }
//
//    @FXML
//    @FXRedirect
//    fun textApp(event: ActionEvent): FXRedirectParam {
//        val test = FXRedirectParam(TestStage::class)
//        test.addParams("msg", "hello")
//        test.addParams("back", {
//            alertUtil.successAlert()
//        })
//        return test
//    }
//
//
    fun openFile(event: ActionEvent?) {
        val file = FileChooser().showOpenDialog(stage)
        if (file != null) {
            EventQueue.invokeLater {
                try {
                    desktop.open(file)
                } catch (ex: IOException) {
                    ex.printStackTrace()
                }
            }
        }
    }
//
//    @FXML
//    @FXRedirect
//    fun PDFApp(event: ActionEvent?): FXRedirectParam {
//        return FXRedirectParam(PDFSearchController::class).apply {
//            ownerRedirect = false
//        }
//    }
//
//    @FXML
//    fun chatApp(event: ActionEvent?) {
//        alertUtil.successAlert("正在开发", null)
//    }
//
//    @FXML
//    @FXRedirect
//    fun CalculatorApp(event: ActionEvent?): FXRedirectParam {
//        return FXRedirectParam(CalculatorController::class).apply {
//            ownerRedirect = false
//        }
//    }

}