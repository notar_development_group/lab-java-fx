package xyz.labmem.modular.main.controller

import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.stage.Modality
import xyz.labmem.core.annotation.FXAspect
import xyz.labmem.core.annotation.FXController
import xyz.labmem.core.base.BaseController
import xyz.labmem.core.util.getResourceFile
import xyz.labmem.modular.main.aspect.MainAspect


@FXController(title = "测试鸭", loadContrName = "AppLoadingController", only = true)
@FXAspect(MainAspect::class)
class TestStage : BaseController() {

    override fun InitStage() {
        stage.initModality(Modality.APPLICATION_MODAL)
        val url = getResourceFile("img/鸡鸭 .png").url
        val image = Image(url.openStream())
        val imageView = ImageView(image)
        root.children.add(imageView)
//        (request.getParams()["input"] as JFXTextField).text = request.getParams()["msg"] as String

    }

    override fun onShow() {
        val redirectController = (request.redirectController as MainController)
        redirectController.stage.hide()
        stage.setOnCloseRequest {
            redirectController.stage.show()
        }
    }
}
