package xyz.labmem

import xyz.labmem.core.reflex.FXApplication


class GluonApplication {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            FXApplication.run(FXAppLauncher::class.java, args)
        }
    }
}