package xyz.labmem

import javafx.application.Application
import javafx.stage.Stage
import xyz.labmem.core.annotation.LabFXApplication
import xyz.labmem.core.reflex.FXApplication

@LabFXApplication(onlyApp = true)
class FXAppLauncher : Application() {


    override fun start(primaryStage: Stage) {
        FXApplication.start(primaryStage,this)
    }

}

