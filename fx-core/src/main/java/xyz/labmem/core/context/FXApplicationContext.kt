package xyz.labmem.core.context

import javafx.application.Application
import xyz.labmem.core.base.BaseController

/**
 * @Author lty
 * @Date 2021/9/14 17:37
 */
class FXApplicationContext {

    companion object {
        var onlyControllers = HashMap<Class<out BaseController>, Any>()

        var controllerClazz = HashMap<String, Class<out BaseController>>()

        var loadControllerClazz = HashMap<String, Class<out BaseController>>()

        var mainControllerClazz: Class<out BaseController>? = null

        @JvmField
        var aspectBeans: HashMap<String, Any> = HashMap()

        var onlyApp: Boolean = false

        var application: Application? = null

        var sysVersion = System.getProperties()["os.name"]
    }

}