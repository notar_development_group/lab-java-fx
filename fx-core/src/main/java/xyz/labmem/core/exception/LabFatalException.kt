package xyz.labmem.core.exception

import cn.hutool.core.exceptions.ExceptionUtil
import cn.hutool.log.StaticLog
import javafx.application.Platform
import xyz.labmem.core.reflex.FXApplication
//import xyz.labmem.core.util.gui.alert.AlertUtil

/**
 * @Author lty
 * @Date 2021/9/15 15:08
 */
class LabFatalException(msg: String, e: Exception?) : RuntimeException(msg, e) {

    constructor(msg: String) : this(msg, null)

    init {
        val error = StringBuilder().apply {
            appendLine()
            appendLine("=============== 致命错误 (LabSysException)===============")
            appendLine("message：$msg")
            e?.let {
                appendLine(ExceptionUtil.getMessage(e))
                it.printStackTrace()
            }
            appendLine("===================== ERROR  END =======================")
        }
        StaticLog.error(error.toString())
        val threadName = Thread.currentThread().name
        if ("JavaFX Application Thread" != threadName) {
            Platform.runLater {
//                AlertUtil().errorAlert(msg, {
//                    FXApplication.close()
//                }, {
                    FXApplication.close()
//                })
            }
        } else
//            AlertUtil().errorAlert(msg, {
//                FXApplication.close()
//            }, {
                FXApplication.close()
//            })
    }

}