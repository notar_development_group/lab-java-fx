package xyz.labmem.core.exception

import cn.hutool.core.exceptions.ExceptionUtil
import cn.hutool.log.StaticLog

/**
 * @Author lty
 * @Date 2021/9/15 14:41
 */
class LabRunException(msg: String?, e: Exception?) : RuntimeException(msg, e) {

    constructor(e: Exception) : this(e.message ?: "运行错误", e)
    constructor(msg: String) : this(msg, null)

    init {
        val error = StringBuilder().apply {
            appendLine()
            appendLine("=============== 运行错误 (LabSysException)===============")
            appendLine("message：$msg")
            e?.let { appendLine(ExceptionUtil.getMessage(e)) }
            appendLine("===================== ERROR  END =======================")
        }
        StaticLog.error(error.toString())
        e?.printStackTrace()
    }

}