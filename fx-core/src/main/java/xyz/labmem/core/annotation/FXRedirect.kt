package xyz.labmem.core.annotation

import java.lang.annotation.Inherited

/**
 * 重定向(只对非FXML controller有效)
 * @Author lty
 * @Date 2021/9/16 15:22
 */
@Inherited
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
annotation class FXRedirect
