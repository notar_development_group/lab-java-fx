package xyz.labmem.core.annotation

import xyz.labmem.core.aspect.BaseFXAspect
import java.lang.annotation.Inherited
import kotlin.reflect.KClass

/**
 * 切面注解(只对非FXML controller有效)
 * @Author lty
 * @Date 2021/9/16 11:31
 */
@Inherited
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class FXAspect(
    val aspectClazz: KClass<out BaseFXAspect>
)
