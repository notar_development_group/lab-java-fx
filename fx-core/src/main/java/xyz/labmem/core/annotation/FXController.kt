package xyz.labmem.core.annotation

import xyz.labmem.core.util.gui.tray.DefaultSystemTray
import xyz.labmem.core.util.gui.tray.SystemTrayInterFace
import java.lang.annotation.Inherited
import kotlin.reflect.KClass

/**
 * 注册controller
 * @Author lty
 * @Date 2021/9/14 15:57
 */
@Inherited
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class FXController(
    val fxmlPath: String = "",
    val title: String = "",
    val icon: String = "",
    val main: Boolean = false,
    val tray: Boolean = false,
    val myTray: KClass<out SystemTrayInterFace> = DefaultSystemTray::class,
    val loadContrName: String = "",
    val only: Boolean = false,
)
