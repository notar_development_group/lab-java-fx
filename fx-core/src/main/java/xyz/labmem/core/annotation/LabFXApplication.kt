package xyz.labmem.core.annotation

import java.lang.annotation.Inherited

/**
 * 注册框架扫描
 * @Author lty
 * @Date 2021/9/14 15:57
 */
@Inherited
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class LabFXApplication(
    vararg val base: String,
    val onlyApp: Boolean = false
)
