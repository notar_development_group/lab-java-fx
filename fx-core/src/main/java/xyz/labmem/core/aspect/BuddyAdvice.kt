package xyz.labmem.core.aspect

import net.bytebuddy.ByteBuddy
import net.bytebuddy.asm.Advice
import net.bytebuddy.matcher.ElementMatchers


/**
 * Byte Buddy 动态代理对象
 */
class BuddyAdvice {

    companion object {

        fun <T> instance(proxyObj: Class<T>): T {
            return ByteBuddy()
                .subclass(proxyObj) // 动态生成proxyObj的子类
                .method(ElementMatchers.any())// 拦截所有方法
                .intercept(Advice.to(FXApplicationAspect::class.java)) // 使用FXApplicationAspect类作为拦截器，Advice是AOP的概念，似乎一般翻译为「通知」？
                .make()//做出
                .load(proxyObj.classLoader)//硬塞给ClassLoader
                .loaded// 拿到Class对象
                .getDeclaredConstructor()
                .newInstance()//初始化对象
        }

    }

}