package xyz.labmem.core.aspect

import java.lang.reflect.Method

/**
 * @Author lty
 * @Date 2021/9/16 11:24
 */
interface BaseFXAspect {

    fun before(target: Any, method: Method, args: Array<out Any>?): Boolean {
        return true
    }

    fun after(target: Any, method: Method, args: Array<out Any>?, returnVal: Any?): Boolean {
        return true
    }

    fun afterException(target: Any, method: Method, args: Array<out Any>?, e: Throwable?): Boolean {
        return true
    }

}