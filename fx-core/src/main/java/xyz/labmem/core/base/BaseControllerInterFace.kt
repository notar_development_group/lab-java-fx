package xyz.labmem.core.base

import javafx.scene.layout.Pane
import javafx.stage.Stage
//import xyz.labmem.core.util.gui.alert.Alert
//import xyz.labmem.core.util.gui.alert.AlertUtil
import xyz.labmem.core.util.gui.loading.Loading
import xyz.labmem.core.util.gui.loading.LoadingAlert
import xyz.labmem.core.util.gui.notice.Notice
import xyz.labmem.core.util.gui.tray.SystemTrayInterFace


interface BaseControllerInterFace {

    fun InitStage()

    fun onShow()

    fun loadingInit()

    var root: Pane

    var stage: Stage

    var request: FXRedirectParam

    var tray: SystemTrayInterFace

    var notice: Notice

    var loadingAlert: LoadingAlert

}