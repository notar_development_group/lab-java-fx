package xyz.labmem.core.base

import javafx.fxml.FXML
import javafx.scene.layout.Pane
import javafx.stage.Stage
import javafx.stage.Window
import kotlinx.coroutines.DelicateCoroutinesApi
import xyz.labmem.core.reflex.FXBuilder
import xyz.labmem.core.util.gui.loading.LoadingAlert
import xyz.labmem.core.util.gui.notice.Notice
import xyz.labmem.core.util.gui.tray.SystemTrayInterFace

abstract class BaseController : BaseControllerInterFace {

    private lateinit var controllerSelf: Any

    //根窗格
    override lateinit var root: Pane

    //窗口
    override lateinit var stage: Stage

    //重定向跳转传参
    override lateinit var request: FXRedirectParam

    //窗口托盘
    override lateinit var tray: SystemTrayInterFace

    override lateinit var notice: Notice

    override lateinit var loadingAlert: LoadingAlert

    /**
     * 获取当前窗口
     */
    fun getWin(): Window? {
        if (null != root.scene) {
            return root.scene.window
        } else if (null != stage.scene) {
            return stage.scene.window
        }
        return null
    }

    @DelicateCoroutinesApi
    fun redirect(param: FXRedirectParam) {
        param.redirectController = controllerSelf
        FXBuilder.startStage(param)
    }

    /**
     * fxml加载
     */
    @FXML
    protected open fun initialize() {
    }

    /**
     * 加载窗口 加载方法
     */
    override fun loadingInit() {}

    /**
     * 初始化controller后执行(适用画窗口)
     */
    override fun InitStage() {}

    /**
     * 窗口show前执行（适用执行其他业务）
     */
    override fun onShow() {}
}