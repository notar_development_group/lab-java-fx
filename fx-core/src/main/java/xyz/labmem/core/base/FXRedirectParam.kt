package xyz.labmem.core.base

import kotlin.reflect.KClass

/**
 * 窗口重定向传值
 * @Author lty
 * @Date 2021/9/16 15:29
 */
class FXRedirectParam(var toControlName: String) {

    var controlClazz: KClass<out BaseController>? = null

    var redirectController: Any? = null

    var ownerRedirect: Boolean = true   //属于父窗口

    constructor(controlClazz: KClass<out BaseController>) : this("") {
        this.controlClazz = controlClazz
    }

    private val params = HashMap<String, Any>()

    fun addParams(key: String, value: Any) {
        params[key] = value
    }

    fun getParams(): HashMap<String, Any> {
        return params
    }

}