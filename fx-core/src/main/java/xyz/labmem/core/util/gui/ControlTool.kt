package xyz.labmem.core.util.gui

import javafx.event.EventHandler
import javafx.scene.Cursor
import javafx.scene.Node
import javafx.scene.control.Control
import javafx.stage.Stage

fun changeCursorsToHand(vararg controls: Control?) {
    for (control in controls) {
        control?.changeCursorsToHand()
    }
}

fun Control.changeCursorsToHand() {
    this.onMouseEntered = EventHandler {
        this.cursor = Cursor.HAND
    }
}


/**
 * 拖动窗口
 * node:绑定拖动元素
 */
fun Stage.dragWin(node: Node) {
    var bX = 0.0
    var yx = 0.0
    node.setOnMouseDragged {
        this.x = it.screenX - bX
        this.y = it.screenY - yx
    }
    node.setOnMousePressed {
        bX = it.x
        yx = it.y
    }
}