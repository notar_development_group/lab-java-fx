package xyz.labmem.core.util.gui.notice

import io.github.palexdev.materialfx.controls.MFXIconWrapper
import io.github.palexdev.materialfx.controls.MFXSimpleNotification
import io.github.palexdev.materialfx.factories.InsetsFactory
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.layout.*


/**
 * @description: do something
 * @author: liutianyu
 * @date: 2022/11/10 13:27
 */
class SimpleNotification(needHeader: Boolean = true) : MFXSimpleNotification() {

    private val headerText: StringProperty = SimpleStringProperty("Notification Header")

    private var icon: MFXIconWrapper = MFXIconWrapper("mfx-bell-alt", 16.0, 32.0)

    private var container = BorderPane()

    init {

//        val action1 = MFXButton("Action 1")
//        val action2 = MFXButton("Action 2")
//        val actionsBar = HBox(15.0, action1, action2)
//        actionsBar.styleClass.add("actions-bar")
//        actionsBar.alignment = Pos.CENTER_RIGHT
//        actionsBar.padding = InsetsFactory.all(5.0)
        container.styleClass.add("notification")
        if (needHeader) {
            val headerLabel = Label()
            headerLabel.textProperty().bind(headerText)
            val header = HBox(15.0, icon, headerLabel)
            header.alignment = Pos.CENTER_LEFT
            header.padding = InsetsFactory.of(5.0, 0.0, 5.0, 0.0)
            header.maxWidth = Double.MAX_VALUE
            container.top = header
        }
        container.style = """
            -fx-background-color: rgba(255, 255, 255, .9);
	        -fx-background-radius: 5;
	        -fx-border-color: #ebebeb;
	        -fx-border-radius: 5;
        """.trimIndent()
        container.autosize()
//        container.bottom = actionsBar
//        container.stylesheets.add(MFXDemoResourcesLoader.load("css/ExampleNotification.css"))
//        container.minWidth = 400.0
//        container.minHeight = 200.0
        content = container
    }

    fun content(node: Region): SimpleNotification {
        container.center = node
        return this
    }

    fun title(title: String): SimpleNotification {
        headerText.set(title)
        return this
    }

    fun titlePropertyBin(title: StringProperty): SimpleNotification {
        headerText.bind(title)
        return this
    }

    fun icon(icon: MFXIconWrapper): SimpleNotification {
        this.icon = icon
        return this
    }

}