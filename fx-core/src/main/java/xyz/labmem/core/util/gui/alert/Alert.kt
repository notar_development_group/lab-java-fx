package xyz.labmem.core.util.gui.alert

import io.github.palexdev.materialfx.controls.MFXButton
import io.github.palexdev.materialfx.controls.MFXScrollPane
import io.github.palexdev.materialfx.dialogs.MFXGenericDialog
import io.github.palexdev.materialfx.dialogs.MFXGenericDialogBuilder
import io.github.palexdev.materialfx.dialogs.MFXStageDialog
import io.github.palexdev.materialfx.enums.ButtonType
import io.github.palexdev.materialfx.enums.ScrimPriority
import io.github.palexdev.materialfx.font.MFXFontIcon
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.*
import javafx.scene.paint.Color
import javafx.scene.text.Font
import javafx.stage.Modality
import javafx.stage.Window


/**
 * 弹窗
 * @Author lty
 * @Date 2021/9/13 11:38
 */
open class Alert(private val window: Window?, val root: Pane?) {

    private var dialogContent: MFXGenericDialog

    private var dialog: MFXStageDialog

    constructor(window: Window?) : this(window, null)
    constructor(root: Pane?) : this(null, root)
    constructor() : this(null, null)

    init {
        dialogContent = MFXGenericDialogBuilder().makeScrollable(true).get()
        dialog = MFXGenericDialogBuilder.build(dialogContent)
            .toStageDialogBuilder().apply {
                window?.let {
                    initOwner(it)
                    initModality(Modality.APPLICATION_MODAL)
                }
                //可拖动
                setDraggable(false)
                setTitle("提示")
                root?.let {
                    setOwnerNode(it)
                }
                setScrimPriority(ScrimPriority.WINDOW)
                setScrimOwner(true)
            }.get()
    }

    fun initialize(): Alert = initialize(null, null)
    fun initialize(dc: MFXGenericDialog?, d: MFXStageDialog?): Alert {
        this.dialogContent = dc ?: MFXGenericDialogBuilder().makeScrollable(true).get()
        this.dialog = d ?: MFXGenericDialogBuilder.build(dialogContent)
            .toStageDialogBuilder().apply {
                window?.let {
                    initOwner(it)
                    initModality(Modality.APPLICATION_MODAL)
                }
                //可拖动
                setDraggable(false)
                setTitle("Dialogs Preview")
                root?.let {
                    setOwnerNode(it)
                }
                setScrimPriority(ScrimPriority.WINDOW)
                setScrimOwner(true)
            }.get()
        return this
    }

    fun show() {
        dialog.showDialog()
    }

    fun close() {
        dialog.close()
    }

    fun getDialog(): MFXStageDialog = dialog

    fun getContent(): MFXGenericDialog = dialogContent

    /**
     * 成功提示模板
     */
    fun toSuccess(msg: String, btn: ((Alert) -> Unit)? = null): Alert = toMsg(AlertType.SUCCESS, msg, btn)


    /**
     * 失败提示模板
     */
    fun toFailed(msg: String, btn: ((Alert) -> Unit)? = null): Alert = toMsg(AlertType.FAILED, msg, btn)

    /**
     * 错误提示模板
     */
    fun toError(msg: String, btn: ((Alert) -> Unit)? = null): Alert = toMsg(AlertType.ERROR, msg, btn)


    fun toMsg(type: AlertType, msg: String = type.defaultMsg(), btn: ((Alert) -> Unit)? = null): Alert {
        return initialize().apply {
            dialogContent.apply {
                minWidth = 300.0
                isShowMinimize = false
                isShowAlwaysOnTop = false
                border = Border(
                    BorderStroke(
                        type.getBorderColor(), BorderStrokeStyle.SOLID,
                        CornerRadii(5.0), BorderWidths(2.0)
                    )
                )
                addActions(
                    MFXButton("确认").apply {
                        buttonType = ButtonType.RAISED
                        style = type.getStyleColor()
                        prefHeight = 30.0
                        prefWidth = 60.0
                        onAction = EventHandler {
                            btn?.let { b -> b(this@Alert) } ?: close()
                        }
                    }
                )
                headerIcon = MFXFontIcon(type.getIcon(), 20.0, type.getColor())
                headerText = type.defaultTitle()
                content = HBox().apply {
                    children.add(ImageView(Image(type.getImg())).apply {
                        fitHeight = 60.0
                        fitWidth = 60.0
                    })
                    children.add(MFXScrollPane(Label(msg).apply {
                        font = Font.font(20.0)
                        isWrapText = true
                    }).apply {
                        HBox.setMargin(this, Insets(50.0, 0.0, 0.0, 10.0))
                        isFitToWidth = true
                    })
                    alignment = Pos.CENTER_LEFT
                }
            }
        }
    }


    /**
     * 普通提示模板
     */
    fun toInfo(title: String = "提示!", content: Region, btns: Array<MFXButton>? = null): Alert {
        return initialize().apply {
            dialogContent.apply {
                prefWidthProperty().bind(content.prefWidthProperty())
                prefHeightProperty().bind(content.prefHeightProperty())
                if (!btns.isNullOrEmpty()) {
                    addActions(*btns)
                } else
                    addActions(
                        MFXButton("好的").apply {
                            style = "outline-button"
                            style = "-fx-background-color: -mfx-purple;-fx-text-fill: white"
                            prefHeight = 30.0
                            prefWidth = 60.0
                            onAction = EventHandler { close() }
                        }
                    )
                headerIcon = MFXFontIcon("mfx-info-circle", 20.0, Color.BLUE)
                headerText = title
                this.content = MFXScrollPane(content).apply {
                    isFitToWidth = true
                }
            }
        }
    }

    /**
     * 弹窗页面
     */
    fun open(title: String = "弹窗", content: Region, btns: Array<MFXButton>?): Alert {
        dialog.title = title
        dialogContent.apply {
            prefWidthProperty().bind(content.prefWidthProperty())
            prefHeightProperty().bind(content.prefHeightProperty())
            if (!btns.isNullOrEmpty()) {
                addActions(*btns)
            }
            headerIcon = null
            headerText = title
            this.content = MFXScrollPane(content).apply {
                isFitToWidth = true
            }
        }
        show()
        return this
    }

}