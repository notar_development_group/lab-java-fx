package xyz.labmem.core.util.gui.tray

import xyz.labmem.core.base.BaseController

/**
 * @Author lty
 * @Date 2021/9/17 13:49
 */
interface SystemTrayInterFace {

    fun listen(controller: BaseController)

}