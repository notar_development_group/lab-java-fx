package xyz.labmem.core.util.gui.loading

import javafx.scene.layout.Pane
import javafx.stage.Window

/**
 * @description: do something
 * @author: liutianyu
 * @date: 2022/11/9 13:32
 */
object LoadingBuild {

    fun buildTypeOne(root: Pane): Loading = Loading(root)

    fun buildTypeTwo(window: Window?): LoadingAlert = LoadingAlert(window)

}

fun Pane.buildTypeOne(): Loading = Loading(this)

fun Window.buildTypeTwo(): LoadingAlert = LoadingAlert(this)