package xyz.labmem.core.util.gui.notice

import io.github.palexdev.materialfx.controls.MFXScrollPane
import io.github.palexdev.materialfx.enums.NotificationPos
import io.github.palexdev.materialfx.notifications.MFXNotificationSystem
import javafx.application.Platform
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.Background
import javafx.scene.layout.HBox
import javafx.scene.layout.Region
import javafx.scene.text.Font
import javafx.stage.Window
import kotlinx.coroutines.withTimeout
import xyz.labmem.core.util.gui.alert.AlertType

/**
 * @description: do something
 * @author: liutianyu
 * @date: 2022/11/10 13:54
 */
class Notice(initOwner: Window?) {

    private var mfxNS: MFXNotificationSystem = MFXNotificationSystem.instance()

    init {
        mfxNS.initOwner(initOwner)
    }

    fun create(content: Region, initOwner: Window, pos: NotificationPos = NotificationPos.TOP_RIGHT) {
        Platform.runLater {
            mfxNS.apply {
                position = pos
                publish(SimpleNotification().content(
                    MFXScrollPane().apply {
                        background= Background.EMPTY
                        isFitToWidth = false
                        this.content = content
                        prefWidthProperty().bind(content.prefWidthProperty().add(10))
                        prefHeightProperty().bind(content.prefHeightProperty().add(10))
                        minHeightProperty().bind(content.minHeightProperty())
                        minWidthProperty().bind(content.minWidthProperty())
                    }
                ))
            }
        }
    }

    fun msg(msg: String, type: AlertType, pos: NotificationPos = NotificationPos.TOP_CENTER) {
        Platform.runLater {
            mfxNS.apply {
                position = pos
                publish(SimpleNotification(needHeader = false).content(HBox().apply {
                    children.add(ImageView(Image(type.getImg())).apply {
                        fitHeight = 18.0
                        fitWidth = 18.0
                        HBox.setMargin(this, Insets(5.0, 0.0, 5.0, 15.0))
                    })
                    val text = Label(msg).apply {
                        font = Font.font(17.0)
                        style = "-fx-text-fill: rgb(${type.getFontColor()});"
                        isWrapText = true
                    }
                    children.add(MFXScrollPane(text).apply {
                        HBox.setMargin(this, Insets(10.0, 10.0, 0.0, 10.0))
                        isFitToWidth = true
                        background= Background.EMPTY
                    })
                    widthProperty().addListener { _, _, n1 ->
                        if (n1.toDouble() >= 790) {
                            prefHeight = 65.0
                        }
                    }
                    alignment = Pos.CENTER_LEFT
                    minWidth = 400.0
                    minHeight = 30.0
                    maxWidth = 800.0
                }))
            }
        }
    }


}