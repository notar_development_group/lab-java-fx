package xyz.labmem.core.util

import com.sun.javafx.stage.WindowHelper
import javafx.stage.Stage
import java.lang.reflect.Method


/**
 * 获取stage句柄 windows
 * @Author lty
 * @Date 2021/12/3 11:29
 */

fun getWindowPointer(stage: Stage): Long? {
    return try {
        val tkStage = WindowHelper.getPeer(stage.scene.window)
        val getPlatformWindow: Method = tkStage.javaClass.getDeclaredMethod("getPlatformWindow")
        getPlatformWindow.isAccessible = true
        val platformWindow: Any = getPlatformWindow.invoke(tkStage)
        val getNativeHandle: Method = platformWindow.javaClass.getMethod("getNativeHandle")
        getNativeHandle.isAccessible = true
        val nativeHandle: Any = getNativeHandle.invoke(platformWindow)
        nativeHandle as Long
    } catch (e: Throwable) {
        System.err.println("Error getting Window Pointer")
        e.printStackTrace()
        null
    }
}