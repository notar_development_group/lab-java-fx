package xyz.labmem.core.util.gui.tray

import com.dustinredmond.fxtrayicon.FXTrayIcon
import io.github.palexdev.materialfx.controls.MFXScrollPane
import javafx.scene.control.Label
import javafx.scene.text.Font
import javafx.stage.Stage
import xyz.labmem.core.base.BaseController
import xyz.labmem.core.reflex.FXApplication
import xyz.labmem.core.util.getResourceFile
import xyz.labmem.core.util.gui.alert.Alert

/**
 * @description: do something
 * @author: liutianyu
 * @date: 2022/11/8 17:13
 */
class DefaultSystemTray : SystemTrayInterFace {

    var trayIcon: FXTrayIcon? = null

    override fun listen(controller: BaseController) {
        val stage = controller.stage
        if (FXTrayIcon.isSupported()) {
            trayIcon = FXTrayIcon.Builder(stage, getResourceFile("icon.jpg").url).apply {
                onAction {
                    showStage(stage)
                }
                menuItem("关于") {
                    showStage(stage)
                    Alert(stage).apply {
                        toInfo(
                            title = "关于",
                            content = Label(" LabApp JAVAFX 框架").apply {
                                font = Font.font(16.0)
                                isWrapText = true
                            }
                        )
                        show()
                    }
                }
                menuItem("退出") {
                    FXApplication.close()
                }
                show()
            }.build()
        } else {
            //系统托盘不支持
            Alert(stage).toFailed("当前系统不支持托盘!").show()
        }

    }

    /**
     * 点击系统托盘,显示界面(并且显示在最前面,将最小化的状态设为false)
     */
    private fun showStage(stage: Stage) {
        //点击系统托盘,
        if (stage.isIconified) {
            stage.isIconified = false
        }
        if (stage.isShowing) {
            stage.show()
        } else
            stage.hide()
        stage.toFront()
    }

    fun winMsg(caption: String?, content: String) {
        trayIcon?.showMessage(caption ?: "LabApp™ 消息！", content)
    }

    fun winMsgInfo(caption: String?, content: String) {
        trayIcon?.showInfoMessage(caption ?: "LabApp™ INFO :", content)
    }

    fun winMsgWarning(caption: String?, content: String) {
        trayIcon?.showWarningMessage(caption ?: "LabApp™ Warning :", content)
    }

    fun winMsgError(caption: String?, content: String) {
        trayIcon?.showErrorMessage(caption ?: "LabApp™ Error :", content)
    }

}