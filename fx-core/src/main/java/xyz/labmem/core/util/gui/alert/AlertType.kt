package xyz.labmem.core.util.gui.alert

import javafx.scene.paint.Color
import xyz.labmem.core.util.getResourceFile
import java.io.InputStream

/**
 * @description: do something
 * @author: liutianyu
 * @date: 2022/11/9 15:38
 */
enum class AlertType {

    SUCCESS, FAILED, ERROR;

    fun defaultMsg(): String {
        return when (this) {
            SUCCESS -> "SUCCESS Alert !"
            FAILED -> "FAILED Alert !"
            ERROR -> "ERROR Alert !"
        }
    }

    fun defaultTitle(): String {
        return when (this) {
            SUCCESS -> "成功提示"
            FAILED -> "失败提示"
            ERROR -> "错误提示"
        }
    }

    fun getImg(): InputStream {
        return when (this) {
            SUCCESS -> getResourceFile("SysStatic/img/success.png").stream
            FAILED -> getResourceFile("SysStatic/img/failed.png").stream
            ERROR -> getResourceFile("SysStatic/img/error.png").stream
        }
    }

    fun getColor(): Color {
        return when (this) {
            SUCCESS -> Color.GREEN
            FAILED -> Color.ORANGE
            ERROR -> Color.RED
        }
    }

    fun getStyleColor(): String {
        return when (this) {
            SUCCESS -> "-fx-background-color: -mfx-green;-fx-text-fill: white"
            FAILED -> "-fx-background-color: -mfx-orange;-fx-text-fill: white"
            ERROR -> "-fx-background-color: -mfx-red;-fx-text-fill: white"
        }
    }

    fun getIcon(): String {
        return when (this) {
            SUCCESS -> "mfx-variant7-mark"
            FAILED -> "mfx-exclamation-circle"
            ERROR -> "mfx-x-circle"
        }
    }

    fun getFontColor(): String {
        return when (this) {
            SUCCESS -> "103,194,58"
            FAILED -> "208,153,99"
            ERROR -> "245,108,108"
        }
    }

    fun getBorderColor(): Color {
        return when (this) {
            SUCCESS -> Color.rgb(103, 194, 58)
            FAILED -> Color.rgb(208, 153, 99)
            ERROR -> Color.rgb(245, 108, 108)
        }
    }

}