package xyz.labmem.core.util.gui.loading

/**
 * @description: do something
 * @author: liutianyu
 * @date: 2022/11/9 14:01
 */
enum class LoadingType {

    BARS, SPINNERS

}